﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MITT_Adytia_Pati_Rangga.Controllers
{
    public class Controller1 : Controller
    {
        // GET: Controller
        public ActionResult Index()
        {
            return View();
        }

        // GET: Controller/Details/5
        public ActionResult Register()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        public ActionResult MasterSkillLevel()
        {
            return View();
        }
        public ActionResult CreateMasterSkillLevel()
        {
            return PartialView("_CreateMasterSkillLevel");
        }
        public ActionResult EditMasterSkillLevel()
        {
            return PartialView("_EditMasterSkillLevel");
        }
        public ActionResult DeleteMasterSkillLevel()
        {
            return PartialView();
        }
        public ActionResult MasterSkill()
        {
            return View();
        }
        public ActionResult CreateMasterSkill()
        {
            return PartialView("_CreateMasterSkill");
        }
        public ActionResult EditMasterSkill()
        {
            return PartialView("_EditMasterSkill");
        }
        public ActionResult DeleteMasterSkill()
        {
            return PartialView();
        }
        public ActionResult UserProfile()
        {
            return View();
        }
        public ActionResult UserSkill()
        {
            return View();
        }
        public ActionResult CreateUserSkill()
        {
            return PartialView("_CreateUserSkill");
        }
        public ActionResult EditUserSkill()
        {
            return PartialView("_EditUserSkill");
        }
        public ActionResult DeleteUserSkill()
        {
            return PartialView();
        }
    }
}
